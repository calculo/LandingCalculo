 
// jQuery for page scrolling feature - requires jQuery Easing plugin
 
$('body').on('click', '.page-scroll a', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top
    }, 1500, 'easeInOutExpo');
    event.preventDefault();
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

$(window).scroll(function (event) {
    var sc = $(window).scrollTop();

    if(sc > 100) {
        $('.navigation').addClass('fixed-navigation');
    }else {
        $('.navigation').removeClass('fixed-navigation');
    }
});

$('.toggle-button').on('click', function() {
    $('.toggle-button').toggleClass('toggleOn');
    $('.content-mobile label').toggleClass('on-right');
    $('ul.nav').toggleClass('show-menu');
    $('body').toggleClass('no-scroll');
});

$('.page-scroll a').on('click', function() {
   $('ul.nav').removeClass('show-menu'); 
   $('.toggle-button').removeClass('toggleOn');
   $('.content-mobile label').removeClass('on-right');
   $('body').removeClass('no-scroll');
});
 
 
function animations() {
    
    animate = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 100,
        mobile: false,
        live: true
    })
    
    animate.init();
    
}

animations();

// Hide Header on on scroll down
// var didScroll;
// var lastScrollTop = 0;
// var delta = 5;
// var navbarHeight = $('.navigation').outerHeight();

// $(window).scroll(function(event){
//     didScroll = true;
// });

// setInterval(function() {
//     if (didScroll) {
//         hasScrolled();
//         didScroll = false;
//     }
// }, 250);

// function hasScrolled() {
//     var st = $(this).scrollTop();
    
//     // Make sure they scroll more than delta
//     if(Math.abs(lastScrollTop - st) <= delta)
//         return;
    
//     // If they scrolled down and are past the navbar, add class .nav-up.
//     // This is necessary so you never see what is "behind" the navbar.
//     if (st > lastScrollTop && st > navbarHeight){
//         // Scroll Down
//         $('.navigation').css('top', '-'+navbarHeight+'px');
//     } else {
//         // Scroll Up
//         if(st + $(window).height() < $(document).height()) {
//             $('.navigation').css('top', '0px');
//         }
//     }
    
//     lastScrollTop = st;
// }


 

$('.content-slider').owlCarousel({
    items:1,
    loop:true,
    autoplay:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true
});
 

$('.box-testimonial').owlCarousel({
    items:1,
    loop:true,
    autoplay:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true
});
